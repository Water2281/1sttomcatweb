import EmailCheck.EmailValidation;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class Servlet1 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String email = req.getParameter("email");
        String gender = req.getParameter("check");
        RequestDispatcher rd = req.getRequestDispatcher("add2");
        if(email.contains("@astanait.edu.kz")){
            if(gender.equals("Female")){
                resp.sendRedirect("https://leadersinheels.com/");
            }
            else{
                req.setAttribute("message","Be brave enough, play kokpar, rugby and hockey!");
                rd.forward(req,resp);
            }
        }
        else{
            req.setAttribute("message","Sorry, you must be a member of Astana IT University Community");
            rd.forward(req,resp);
        }
    }
}
